require_relative 'rclrb_spec'

require 'std_msgs/msg'

RSpec.describe "subscription" do
  it "can subscribe to sring messages" do
    node = Rclrb::Node.new("subscriber")
    f = Rclrb::Future.new
    s = node.create_subscription(StdMsgs::Msg::String, "topic", 1) { |msg| f.set_result msg.data }
    `ros2 topic pub -1 /topic std_msgs/String "data: 'Hello World!'"`
    Rclrb::WaitSet.wait_for(s) { s.spin; f.has_result? }
    expect(f.result).to eq("Hello World!")
  end
  it "can subscribe to messages with an array" do
    node = Rclrb::Node.new("subscriber")
    f = Rclrb::Future.new
    s = node.create_subscription(StdMsgs::Msg::UInt8MultiArray, "topic_ma", 1) { |msg| f.set_result msg.data }
    `ros2 topic pub -1 /topic_ma std_msgs/UInt8MultiArray "{layout: {dim: [{label: "test", size: 5, stride: 3}], data_offset: 32}, data: [10, 30]}"`
    Rclrb::WaitSet.wait_for(s) { s.spin; f.has_result? }
    expect(f.result).to eq([10, 30])
  end
end
