require 'rspec'
require 'rclrb'
require 'timeout'
time_limit = 30

RSpec.configure do |config|
  config.before(:all) { }
  config.after(:all) {
    GC.start
  }
  config.before(:suite) do
    Rclrb.init
  end
  config.after(:suite) do
    Rclrb.shutdown
  end
  config.around(:each) do |example|
    Timeout::timeout(time_limit) {
      example.run
    }
  end
end

