require_relative 'rclrb_spec'

require 'std_msgs/msg'

RSpec.describe "publisher" do
  it "can publish sring messages" do
    node = Rclrb::Node.new("publisher")
    f = Rclrb::Future.execute { `ros2 topic echo --once /topic std_msgs/String` }
    pub = node.create_publisher StdMsgs::Msg::String, "topic", 1
    while pub.subscription_count == 0 
    end
    string_msg = StdMsgs::Msg::String.new
    string_msg.data = "Hello World!"
    pub.pub string_msg
    expect(f.result).to end_with("data: Hello World!\n---\n")
  end
  it "can publish messages with array" do
    node = Rclrb::Node.new("publisher")
    f = Rclrb::Future.execute { `ros2 topic echo --once /topic_ma std_msgs/UInt8MultiArray` }
    pub = node.create_publisher StdMsgs::Msg::UInt8MultiArray, "topic_ma", 1
    while pub.subscription_count == 0 
    end
    msg = StdMsgs::Msg::UInt8MultiArray.new
    dim = StdMsgs::Msg::MultiArrayDimension.new
    dim.label = "test"
    dim.size  = 5
    dim.stride = 3
    msg.layout.dim.append dim
    msg.data = [10, 30]
    pub.pub msg
    expect(f.result).to end_with("layout:\n  dim:\n  - label: test\n    size: 5\n    stride: 3\n  data_offset: 0\ndata:\n- 10\n- 30\n---\n")
  end
end
