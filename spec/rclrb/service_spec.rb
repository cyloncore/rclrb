require_relative 'rclrb_spec'

require 'example_interfaces/srv'

RSpec.describe "service" do
  it "can provide an addition service that can be called from the command line" do
    node = Rclrb::Node.new("service")
    sf = Rclrb::Future.new
    s = node.create_service(ExampleInterfaces::Srv::AddTwoInts, 'add_two_ints') do |request, response|
      response.sum = request.a + request.b
      sf.set_result response.sum
    end
    cf = Rclrb::Future.execute do
      c = `ros2 service call /add_two_ints example_interfaces/srv/AddTwoInts "{a: 2, b: 3}"`
      c
    end
    Rclrb::WaitSet.wait_for(s) { s.spin; sf.has_result? }
    expect(sf.result).to eq(5)
    expect(cf.result).to end_with("requester: making request: example_interfaces.srv.AddTwoInts_Request(a=2, b=3)\n\nresponse:\nexample_interfaces.srv.AddTwoInts_Response(sum=5)\n\n")
  end
  it "can provide an addition service and call it" do
    node = Rclrb::Node.new("service")
    sf = Rclrb::Future.new
    s = node.create_service ExampleInterfaces::Srv::AddTwoInts, 'add_two_ints' do |request, response|
      response.sum = request.a + request.b
      sf.set_result response.sum
    end
    c = node.create_client ExampleInterfaces::Srv::AddTwoInts, 'add_two_ints'
    req = ExampleInterfaces::Srv::AddTwoInts::Request.new()
    req.a = 4
    req.b = 5
    cf = c.call_async req

    Rclrb::WaitSet.wait_for(s) { s.spin; sf.has_result? }
    expect(sf.result).to eq(9)
    expect(cf.result.sum).to eq(9)
  end
end
