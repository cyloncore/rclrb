require_relative 'rclrb_spec'

require 'std_msgs/msg'
require 'std_srvs/srv'

def prepare_message type, &block
  msg = type.new
  block.call msg
  ros_data = type.get_ros_message msg
  msg_back = type.parse_ros_message ros_data
  type.destroy_ros_message ros_data
  return msg_back
end

RSpec.describe "messages" do
  it "works with std_msg/string" do
    msg = prepare_message(StdMsgs::Msg::String) { |msg| msg.data = "Hello World!" }
    expect(msg.data).to eq("Hello World!")
  end
  it "works with std_msg/float64" do
    msg = prepare_message(StdMsgs::Msg::Float64) { |msg| msg.data = 12.52 }
    expect(msg.data).to eq(12.52)
  end
  it "works with std_msg/Header" do
    msg = prepare_message(StdMsgs::Msg::Header) { |msg| msg.stamp = Rclrb::Time.from_sec 12.5; msg.frame_id = "hello" }
    expect(msg.stamp.to_sec).to eq(12.5)
    expect(msg.frame_id).to eq("hello")
  end
  it "works with std_msg/UInt32MultiArray" do
    msg = prepare_message(StdMsgs::Msg::UInt32MultiArray) do |msg|
      dim = StdMsgs::Msg::MultiArrayDimension.new
      dim.label = "test"
      dim.size  = 5
      dim.stride = 3
      msg.layout.dim.append(dim)
      msg.data = [10, 30]
    end
    expect(msg.layout.dim.length).to eq(1)
    expect(msg.layout.dim[0].label).to eq("test")
    expect(msg.layout.dim[0].size).to eq(5)
    expect(msg.layout.dim[0].stride).to eq(3)
    expect(msg.data).to eq([10, 30])
  end
  it "works with std_msg/Empty" do
    m = StdMsgs::Msg::Empty.new
    StdMsgs::Msg::Empty.get_ros_message m
  end
  it "works with std_srv/Empty" do
    req = StdSrvs::Srv::Empty::Request.new
    StdSrvs::Srv::Empty::Request.get_ros_message req
  end
  it "can be created from dict" do
    m = StdMsgs::Msg::String.create_from_dict({"data" => "hello"})
    expect(m.data).to eq("hello")
  end
end
