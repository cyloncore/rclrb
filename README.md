![rclrb banner](data/images/rclrb-banner.png)
=============================================

Ruby bindings for [ROS2](https:///www.ros.org) RCL libary.

It currently support the following features:

* Publisher/Subscriber to topics
* Service Server/Client
* Timers and Clock
* Multi-threading

Examples are available in a ROS package: [rclrb examples](https://gitlab.com/rosrb/rclrb_examples).

Installation
============

Using gem
---------

`rclrb` can be installed with gem:

```bash
gem install rclrb
```

In a colcon workspace
---------------------

`rclrb` can be built as part of a ROS2 colcon workspace, it requires the package `ament_cmake_ruby` to be built and installed in the workspace:

```bash
git clone https://gitlab.com/rosrb/ament_cmake_ruby.git
git clone https://gitlab.com/rosrb/rclrb.git
```

Changes
=======

1.2.0
-----

* Make it possible to create msg/srv from a dictionary
* Remove ROS arguments from the command line arguments
* Fix dynamic creation of subscriptions/publishers/services
* Fix publishing of array of strings/time
* Fix std_msgs/Empty

1.1.0
-----

* Performance improvements for arrays
* Add function to access node namespace
* Fix parsing of messages with dependencies in the same package
* Fix parsing of constants

1.0.0
-----

* Initial release with support for subscriber, publishers, service clients/servers, and timers.
