require 'erb'

require_relative 'rclrb/common'
require_relative 'rclrb/internal'
require_relative 'rclrb/version'

require_relative 'rclrb/capi'

require_relative 'rclrb/callback_group'
require_relative 'rclrb/clock'
require_relative 'rclrb/client'
require_relative 'rclrb/init'
require_relative 'rclrb/executor'
require_relative 'rclrb/fields'
require_relative 'rclrb/future'
require_relative 'rclrb/guard_condition'
require_relative 'rclrb/interfaces'
require_relative 'rclrb/node'
require_relative 'rclrb/publisher'
require_relative 'rclrb/qos'
require_relative 'rclrb/subscription'
require_relative 'rclrb/time'
require_relative 'rclrb/timer'
require_relative 'rclrb/service'
require_relative 'rclrb/wait_set'

module Kernel
  # make an alias of the original require
  alias_method :rclrb_original_require, :require

  # rewrite require
  LoadedRosInterfacePackages = []
  def require name
    begin
      rclrb_original_require name
    rescue LoadError
      unless LoadedRosInterfacePackages.include? name
        raise unless Rclrb::Interfaces.load_definitions name
        LoadedRosInterfacePackages.append(name)
      end
    end
  end
end
