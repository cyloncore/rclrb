module Rclrb
  ##
  # Uncamelize a string, for instance transform "ThisIsAnExample" to "this_is_an_example" 
  def Rclrb.uncamelize str
    return str.gsub(/::/, '/')
              .gsub(/([A-Z]+)([A-Z][a-z])/,'\1_\2')
              .gsub(/([a-z\d])([A-Z])/,'\1_\2')
              .tr("-", "_")
              .downcase
  end
  ##
  # Camelize a string, for instance transform "this_is_an_example" to "ThisIsAnExample" 
  def Rclrb.camelize str
    return str.split("_").map(&:capitalize).join
  end
end
