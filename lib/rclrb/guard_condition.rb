module Rclrb
  class GuardCondition
    attr_reader :guard_condition_handle
    def initialize
      @guard_condition_handle = CApi.rcl_get_zero_initialized_guard_condition
      CApi.handle_result CApi.rcl_guard_condition_init(@guard_condition_handle, Rclrb.rcl_context, CApi.rcl_guard_condition_get_default_options)
    end
    rclrb_finalize_with :@guard_condition_handle do |guard_condition_handle|
      CApi.handle_result CApi.rcl_guard_condition_fini(guard_condition_handle)
    end
    def trigger()
      CApi.handle_result CApi.rcl_trigger_guard_condition(@guard_condition_handle)
    end
    def spin(wait_set = nil)
      wait_set.add self if wait_set
    end
  end
end
