module Rclrb
  ##
  # Represent a ROS publisher
  class Publisher
    attr_reader :msg_type
    # Construct a new publisher, this should not be called directly, instead use Node.create_publisher.
    def initialize(publisher_handle, node_handle, msg_type, topic, qos_profile, callback_group, event_callbacks)
      @publisher_handle = publisher_handle
      @node_handle = node_handle
      @msg_type = msg_type
      @topic = topic
      @qos_profile = qos_profile

      publisher_ops = CApi.rcl_publisher_get_default_options()
      publisher_ops[:qos] = QoSProfile.get_profile(qos_profile).ros_profile
      CApi.handle_result(CApi.rcl_publisher_init(@publisher_handle, node_handle, msg_type.type_support(), @topic, publisher_ops))
    end

    rclrb_finalize_with :@publisher_handle, :@node_handle do |publisher_handle, node_handle|
      CApi.handle_result CApi.rcl_publisher_fini publisher_handle, node_handle
    end

    ##
    # Publish a message. If +msg+ has the wrong type, an exception is triggered.
    def pub(msg)
      raise InvalidMessageTypeError.new msg.class, @msg_type unless msg.kind_of? @msg_type
      ros_msg = @msg_type.get_ros_message msg
      CApi.handle_result(CApi.rcl_publish(@publisher_handle, ros_msg, nil), lambda { @msg_type.destroy_ros_message ros_msg})
    end

    ##
    # Return the number of subscription to that topic.
    def subscription_count()
      count = CApi::SizeTPtr.new
      CApi.handle_result CApi.rcl_publisher_get_subscription_count(@publisher_handle, count)
      return count[:value]
    end
  end
end
  