module Rclrb
  module QosReliabilityPolicy
    ## Implementation specific default
    SystemDefault = :RMW_QOS_POLICY_RELIABILITY_SYSTEM_DEFAULT
    ## Guarantee that samples are delivered, may retry multiple times
    Reliable = :RMW_QOS_POLICY_RELIABILITY_RELIABLE
    ## Attempt to deliver samples, but some may be lost if the network is not robust
    BestEffort = :RMW_QOS_POLICY_RELIABILITY_BEST_EFFORT
    ## Reliability policy has not yet been set
    Unknown = :RMW_QOS_POLICY_RELIABILITY_UNKNOWN
  end
  module QosDurabilityPolicy
    ## Implementation specific default
    SystemDefault = :RMW_QOS_POLICY_DURABILITY_SYSTEM_DEFAULT
    ## The rmw publisher is responsible for persisting samples for “late-joining” subscribers
    TransientLocal = :RMW_QOS_POLICY_DURABILITY_TRANSIENT_LOCAL
    ## Samples are not persistent
    Volatile = :RMW_QOS_POLICY_DURABILITY_VOLATILE
    ## History policy has not yet been set
    Unknown = :RMW_QOS_POLICY_DURABILITY_UNKNOWN
  end
  module QosLivelinessPolicy
    ## Implementation specific default
    SystemDefault = :RMW_QOS_POLICY_LIVELINESS_SYSTEM_DEFAULT
    ## The signal that establishes a Topic is alive comes from the ROS rmw layer
    Automatic = :RMW_QOS_POLICY_LIVELINESS_AUTOMATIC
    ## The signal that establishes a Topic is alive is at the Topic level. Only publishing a message
    # on the Topic or an explicit signal from the application to assert liveliness on the Topic
    # will mark the Topic as being alive.
    ManualByTopic = :RMW_QOS_POLICY_LIVELINESS_MANUAL_BY_TOPIC
    ## History policy has not yet been set
    Unknown = :RMW_QOS_POLICY_LIVELINESS_UNKNOWN
  end
  module QosHistoryPolicy
    ## Implementation default for history policy
    SystemDefault = :RMW_QOS_POLICY_HISTORY_SYSTEM_DEFAULT
    ## Only store up to a maximum number of samples, dropping oldest once max is exceeded
    KeepLast = :RMW_QOS_POLICY_HISTORY_KEEP_LAST
    ## Store all samples, subject to resource limits
    KeepAll = :RMW_QOS_POLICY_HISTORY_KEEP_ALL
    # History policy has not yet been set
    Unknown = :RMW_QOS_POLICY_HISTORY_UNKNOWN
  end
  ##
  # Represent the QoS profile of a topic, service or action. Look at QoSProfileSensorData,
  # QoSProfileParameters, QoSProfileDefault, QoSProfileServicesDefault, QoSProfileParameterEvents,
  # or QoSProfileSystemDefault.
  class QoSProfile
    attr_reader :ros_profile
    #
    #
    # Parameters:
    # * +depth+ Size of the message queue.
    # * +history+ History QoS policy setting
    # * +reliability+ Reliabiilty QoS policy setting
    # * +durability+ Durability QoS policy setting
    # * +deadline+ The period at which messages are expected to be sent/received
    # * +lifespan+ The age at which messages are considered expired and no longer valid
    # * +liveliness+ Liveliness QoS policy setting
    # * +liveliness_lease_duration+ The time within which the RMW node or publisher must show that it is alive
    # * +avoid_ros_namespace_conventions+ If true, any ROS specific namespacing conventions will be circumvented
    def initialize(depth:, history: QosHistoryPolicy::KeepLast, reliability: QosReliabilityPolicy::Reliable,
                   durability: QosDurabilityPolicy::Volatile, deadline: nil, lifespan: nil,
                   liveliness: QosLivelinessPolicy::SystemDefault, liveliness_lease_duration: nil,
                   avoid_ros_namespace_conventions: false)
      @ros_profile = CApi::RmwQoSProfileT.new
      @ros_profile[:depth] = depth
      @ros_profile[:history] = history
      @ros_profile[:reliability] = reliability
      @ros_profile[:durability] = durability
      unless deadline.nil?
        s, n = deadline.to_sec_nsec
        @ros_profile[:deadline][:sec] = s
        @ros_profile[:deadline][:nsec] = n
      end
      unless lifespan.nil?
        s, n = lifespan.to_sec_nsec
        @ros_profile[:lifespan][:sec] = s
        @ros_profile[:lifespan][:nsec] = n
      end
      @ros_profile[:lifespan]
      @ros_profile[:liveliness] = liveliness
      unless liveliness_lease_duration.nil?
        s, n = liveliness_lease_duration.to_sec_nsec
        @ros_profile[:liveliness_lease_duration][:sec] = s
        @ros_profile[:liveliness_lease_duration][:nsec] = n
      end
    end
    def QoSProfile.get_profile p
      return p if p.kind_of? QoSProfile
      return QoSProfile.new depth: p 
    end
  end

  ## Qos profile to be used for sensor data,
  QoSProfileSensorData = QoSProfile.new depth: 5, reliability: QosReliabilityPolicy::BestEffort

  ## Qos profile to be used for parameters.
  QoSProfileParameters = QoSProfile.new depth: 1000

  ## Default Qos profile for topics.
  QoSProfileDefault = QoSProfile.new depth: 10

  ## Default Qos profile for services.
  QoSProfileServicesDefault = QoSProfile.new depth: 10

  ## Default Qos profile for events.
  QoSProfileParameterEvents = QoSProfile.new depth: 1000

  ## Default Qos profile using system default (RMW implementation specific).
  QoSProfileSystemDefault = QoSProfile.new depth: 5, history: QosHistoryPolicy::SystemDefault,
                  reliability: QosReliabilityPolicy::SystemDefault,
                  durability: QosDurabilityPolicy::SystemDefault,
                  liveliness: QosLivelinessPolicy::SystemDefault

end