module Rclrb
  ##
  # Represent a ROS Timer
  class Timer
    attr_reader :timer_handle
    # Construct a new timer, this should not be called directly, instead use Node.create_subscription.
    def initialize(handle, node_handle, timer_period_sec, callback, callback_group, clock)
      @callback_proc = Proc.new do |timer, time|
        callback.call time
      end
      @timer_handle = handle
      @node_handle = node_handle
      callback_group.add self

      CApi.handle_result(CApi.rcl_timer_init(handle, clock.handle, Rclrb.rcl_context, timer_period_sec*1e9, @callback_proc, Rclrb.rcl_allocator))
      Rclrb.rcl_signal_guard_condition.trigger
    end

    rclrb_finalize_with :@timer_handle, :@node_handle do |timer_handle, node_handle|
      CApi.handle_result CApi.rcl_timer_fini timer_handle, node_handle
    end

    def spin(wait_set = nil)
      wait_set.add self if wait_set
      is_ready = CApi::BoolPtr.new
      CApi.handle_result(CApi.rcl_timer_is_ready @timer_handle, is_ready)
      if is_ready[:value]
        CApi.handle_result(CApi.rcl_timer_call @timer_handle)
      end
    end
  end
end
