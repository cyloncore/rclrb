module Rclrb
  ##
  # Represent a clock, instead of creating directly, use RosClock or SystemClock
  class Clock
    attr_reader :handle
    def initialize(type)
      @handle = CApi::RclClockT.new
      CApi.handle_result(CApi.rcl_clock_init(type, @handle, CApi.rcutils_get_default_allocator()))
    end
  end
  ##
  # Represent a clock with Ros time (either system or simulated time)
  RosClock = Clock.new(:RCL_ROS_TIME)
  ##
  # Represent a clock that is always set to system time
  SystemClock = Clock.new(:RCL_SYSTEM_TIME)
end
