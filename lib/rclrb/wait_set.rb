module Rclrb
  ##
  # This class is use to Wait for events.
  class WaitSet
    def initialize
      @subscriptions = []
      @services = []
      @clients = []
      @timers = []
    end
    ##
    # Wait for an event on +ẃhat+, every +timeout+ evaluate +block+ and if the value
    # returned is true, then exit the function otherwise continue waiting. 
    def WaitSet.wait_for what, timeout = -1, &block
      ws = WaitSet.new
      ws.add what
      until block.call
        ws.wait timeout
      end
    end
    ##
    # Add an item (Subscription, Service...) to the wait set
    def add(what)
      if what.kind_of? Subscription
        @subscriptions.append what
      elsif what.kind_of? Service
        @services.append what
      elsif what.kind_of? Client
        @clients.append what
      elsif what.kind_of? Timer
        @timers.append what
      end
    end
    ##
    # Wait until +timeout+ or an event occured in one of the item added with add.
    # If +timeout+ is equalt to -1, this function will block until an event occured or indefinitely.
    def wait(timeout = -1)
      ws = CApi.rcl_get_zero_initialized_wait_set
      CApi.handle_result CApi.rcl_wait_set_init(ws, @subscriptions.size, 1, @timers.size, @clients.size, @services.size, 0, Rclrb.rcl_context, Rclrb.rcl_allocator)
      begin
        @subscriptions.each() { |x| CApi.handle_result CApi.rcl_wait_set_add_subscription(ws, x.subscription_handle, nil)}
        CApi.handle_result CApi.rcl_wait_set_add_guard_condition(ws, Rclrb.rcl_signal_guard_condition.guard_condition_handle, nil)
        @services.each() { |x| CApi.handle_result CApi.rcl_wait_set_add_service(ws, x.service_handle, nil)}
        @clients.each() { |x| CApi.handle_result CApi.rcl_wait_set_add_client(ws, x.client_handle, nil)}
        @timers.each() { |x| CApi.handle_result CApi.rcl_wait_set_add_timer(ws, x.timer_handle, nil)}
        # Wait has to run in a seperate thread to allow for signals to work
        status = nil
        t = Thread.new do
          status = CApi.rcl_wait(ws, timeout)
        end
        t.join
        if status != CApi::RCL_RET_OK and status != CApi::RCL_RET_TIMEOUT
          CApi.handle_result status
        end
      rescue RclError
        CApi.handle_result CApi.rcl_wait_set_fini(ws)
        raise
      end
      CApi.handle_result CApi.rcl_wait_set_fini(ws)
    end
  end
end
