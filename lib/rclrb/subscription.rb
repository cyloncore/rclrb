module Rclrb
  ##
  # Represent a ROS subscription to a topic
  class Subscription
    attr_reader :msg_type, :subscription_handle
    # Construct a new subscription, this should not be called directly, instead use Node.create_subscription.
    def initialize(subscription_handle, node_handle, msg_type, topic, callback, qos_profile, callback_group, event_callbacks, raw)
      @subscription_handle = subscription_handle
      @node_handle = node_handle
      @msg_type = msg_type
      @topic = topic
      @callback = callback
      @qos_profile = qos_profile
      @raw = raw
      callback_group.add self
      
      subscription_ops = CApi.rcl_subscription_get_default_options()
      subscription_ops[:qos] = QoSProfile.get_profile(qos_profile).ros_profile
      CApi.handle_result CApi.rcl_subscription_init(@subscription_handle, node_handle, msg_type.type_support(), @topic, subscription_ops)
      Rclrb.rcl_signal_guard_condition.trigger
    end
    rclrb_finalize_with :@subscription_handle, :@node_handle do |subscription_handle, node_handle|
      CApi.handle_result CApi.rcl_subscription_fini(subscription_handle, node_handle)
    end
    def raw?
      return @raw
    end

    def spin(wait_set = nil)
      wait_set.add self if wait_set
      message_info = CApi::RmwMessageInfoT.new
      data = @msg_type::FFIType.new
      status = CApi.rcl_take @subscription_handle, data, message_info, nil

      if status == CApi::RCL_RET_OK
        msg = @msg_type.parse_ros_message data
        @msg_type.destroy_ros_message data
        @callback.call(msg)
      elsif status == CApi::RCL_RET_SUBSCRIPTION_TAKE_FAILED
        # no message received
      else
        CApi.handle_result status
      end
    end
  end
end
