module Rclrb
  #
  # Define a future
  #
  # 
  class Future
    ##
    # Create a future with a nil result
    def initialize
      @result = nil
      @observers = []
      @mutex = Mutex.new
      @resource = ConditionVariable.new
      @exception = nil
    end
    ##
    # Execute the block in a thread and return a Future.
    # The block is expected to return a value which is set using set_value.
    def Future.execute(&block)
      f = Future.new
      Thread.new do
        begin
          f.set_result block.call
        rescue => ex
          f.set_exception ex
        end
      end
      return f
    end
    ##
    # Add an observer, which is executed once the result is set.
    def add_observer(&block)
      @mutex.synchronize do
        if @result.nil?
          @observers.push block
        else
          block.call @result
        end
      end
    end
    ##
    # Wait undefinitely for a result.
    def wait_for_result()
      @mutex.synchronize do
        while @result.nil?
          raise @exception unless @exception.nil?
          @resource.wait(@mutex)
        end
      end
    end
    ##
    # Returns true if it already has a result.
    def has_result?
      return not(@result.nil?)
    end
    ##
    # Set that an exception occurs
    def set_exception(ex)
      @mutex.synchronize do
        @exception = ex
        @resource.signal
      end
    end
    ##
    # Set the result of the future
    def set_result(result)
      raise RclError.new "Result is already set" unless @result.nil?
      @mutex.synchronize do
        @result = result
        @resource.signal
      end
      @observers.each() { |o| o.call Time.now, @result }
      @observers = []
    end
    ##
    # Wait for the result and return it
    def result()
      wait_for_result()
      return @result
    end
  end
end
