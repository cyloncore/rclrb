module Rclrb
  ##
  # Represent a ROS service
  class Service
    attr_reader :srv_type, :service_handle
    # Construct a new service, this should not be called directly, instead use Node.create_service.
    def initialize(service_handle, node_handle, srv_type, srv_name, callback, qos_profile, callback_group)
      @service_handle = service_handle
      @srv_type = srv_type
      @srv_name = srv_name
      @callback = callback
      @callback_group = callback_group
      @qos_profile = qos_profile
      callback_group.add self

      service_ops = CApi.rcl_service_get_default_options()
      service_ops[:qos] = QoSProfile.get_profile(qos_profile).ros_profile
      CApi.handle_result CApi.rcl_service_init(@service_handle, node_handle, srv_type.type_support(), @srv_name, service_ops)
      Rclrb.rcl_signal_guard_condition.trigger
    end

    rclrb_finalize_with :@service_handle, :@node_handle do |service_handle, node_handle|
      CApi.handle_result CApi.rcl_service_fini service_handle, node_handle
    end

    def spin(wait_set = nil)
      wait_set.add self if wait_set
      request_id = CApi::RmwRequestIdT.new
      ros_req = @srv_type::Request::FFIType.new
      status = CApi.rcl_take_request @service_handle, request_id, ros_req

      if status == CApi::RCL_RET_OK
        request = @srv_type::Request.parse_ros_message ros_req
        @srv_type::Request.destroy_ros_message ros_req
        response = @srv_type::Response.new
        @callback.call request, response
        ros_resp = @srv_type::Response.get_ros_message response
        CApi.handle_result CApi.rcl_send_response(@service_handle, request_id, ros_resp), lambda { @srv_type::Response.destroy_ros_message ros_resp}

      elsif status == CApi::RCL_RET_SERVICE_TAKE_FAILED
        # no service received
      else
        CApi.handle_result(status)
      end
    end
  end
end
