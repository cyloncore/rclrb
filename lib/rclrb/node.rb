module Rclrb
  ##
  # The node class is the center piece of RCL, it allows to create subscriptions, publishers timers,
  # and services server and clients.
  class Node
    attr_reader :subscriptions, :timers, :services, :default_callback_group
    ##
    # Create a node with the given +name+,
    def initialize(name)
      @node_handle = CApi::RclNodeT.new()
      @services = []
      @subscriptions = []
      @timers = []
      options = CApi.rcl_node_get_default_options()
      @default_callback_group = MutuallyExclusiveCallbackGroup.new
      CApi.handle_result(CApi.rcl_node_init(@node_handle, name, "", Rclrb.rcl_context(), options))
    end

    rclrb_finalize_with :@node_handle do |node_handle|
      CApi.handle_result CApi.rcl_node_fini node_handle
    end
    
    ##
    # Get the namespace for this node
    def get_namespace
      return CApi.rcl_node_get_namespace @node_handle
    end

    ##
    # Create a new Publisher and return it.
    #
    # Parameters:
    # * +msg_type+ The type of ROS messages the publisher will publish.
    # * +topic+ (string) The name of the topic the publisher will publish to.
    # * +qos_profile+ (QoSProfile or int) A QoSProfile or a history depth to apply to the publisher. In the case that a history depth is provided, the QoS history is set to RMW_QOS_POLICY_HISTORY_KEEP_LAST, the QoS history depth is set to the value of the parameter, and all other QoS settings are set to their default values.
    # * +callback_group+ (optional CallbackGroup) The callback group for the publisher’s event handlers. If None, then the node’s default callback group is used.
    def create_publisher(msg_type, topic, qos_profile, callback_group=nil, event_callbacks=nil)
      callback_group = @default_callback_group if callback_group.nil?
      ## * +event_callbacks+ (Optional) – User-defined callbacks for middleware events.
      if event_callbacks != nil
        raise RclError.new("event_callbacks are not supported yet")
      end
      handle = CApi::rcl_get_zero_initialized_publisher()
      publisher = Publisher.new(handle, @node_handle, msg_type, topic, qos_profile, callback_group, event_callbacks)
      return publisher
    end

    ##
    # Create a new Subscription and return it. This function will also add the Subscription object to @subscriptions.
    # To unsubscribe it is necesserary to call tje unsubscribe function with the handle to the subscriber.
    #
    # Parameters:
    # * +msg_type+ The type of ROS messages the subscription will subscribe to.
    # * +topic+ (string) – The name of the topic the subscription will subscribe to.
    # * +callback+ (block) – A user-defined callback function that is called when a message is received by the subscription.
    # * +qos_profile+ (QoSProfile or int) – A QoSProfile or a history depth to apply to the subscription. In the case that a history depth is provided, the QoS history is set to RMW_QOS_POLICY_HISTORY_KEEP_LAST, the QoS history depth is set to the value of the parameter, and all other QoS settings are set to their default values.
    # * +callback_group (optional CallbackGroup) – The callback group for the subscription. If None, then the nodes default callback group is used.
    def create_subscription(msg_type, topic, qos_profile, callback_group = nil, event_callbacks = nil, raw = false, &callback)
      callback_group = @default_callback_group if callback_group.nil?
      # * +event_callbacks (Optional[SubscriptionEventCallbacks]) – User-defined callbacks for middleware events.
      if event_callbacks != nil
        raise RclError.new("event_callbacks are not supported yet")
      end
      # * +raw (bool) – If True, then received messages will be stored in raw binary representation.
      if raw != false
        raise RclError.new("raw is not supported yet")
      end

      handle = CApi::rcl_get_zero_initialized_subscription()
      subscription = Subscription.new(handle, @node_handle, msg_type, topic, callback, qos_profile, callback_group, event_callbacks, raw)
      @subscriptions.append subscription
      return subscription
    end

    ##
    # Unsubscribe
    # * +subscription+ a subscription created by create_subscription
    def remove_subscription(subscription)
      @subscriptions.remove subscription
    end

    ##
    # Create a new service server.
    #
    # Parameters:
    # * +srv_type+: The service type.
    # * +srv_name+ (str): The name of the service.
    # * +callback+ (block): A user-defined callback function that is called when a service request received by the server, it expects a request and response argument.
    # * +qos_profile+ (QoSProfile): The quality of service profile to apply the service server.
    # * +callback_group+ (optional CallbackGroup): The callback group for the service server. If None, then the nodes default callback group is used.
    def create_service(srv_type, srv_name, qos_profile=QoSProfileServicesDefault, callback_group=nil, &callback)
      callback_group = @default_callback_group if callback_group.nil?
      handle = CApi::rcl_get_zero_initialized_service()
      service = Service.new(handle, @node_handle, srv_type, srv_name, callback, qos_profile, callback_group)
      @services.append(service)
      return service
    end

    ##
    # Unsubscribe
    # * +service+ a service created by create_service
    def remove_service(service)
      @services.remove service
    end

    ##
    # Create a new service client.
    # Parameters:
    # * +srv_type+: The service type.
    # * +srv_name+ (str): The name of the service.
    # * +qos_profile+ (optional QoSProfile): The quality of service profile to apply the service client.
    # * +callback_group+ (optional CallbackGroup): The callback group for the service client. If None, then the nodes default callback group is used
    def create_client(srv_type, srv_name, qos_profile=QoSProfileServicesDefault)
      handle = CApi::rcl_get_zero_initialized_client()
      client = Client.new(handle, @node_handle, srv_type, srv_name, qos_profile)
      return client
    end

    ##
    # Create a new timer.
    # The timer will be started and every timer_period_sec number of seconds the provided callback function will be called.
    #
    # Parameters:
    # * +timer_period_sec+ (float): The period (s) of the timer.
    # * +callback+ (block): A user-defined callback function that is called when the timer expires.
    # * +callback_group+ (optional CallbackGroup): The callback group for the timer. If None, then the nodes default callback group is used.
    # * +clock+ (optional Clock): The clock which the timer gets time from.
    def create_timer(timer_period_sec, callback_group=nil, clock=Rclrb::RosClock, &callback)
      callback_group = @default_callback_group if callback_group.nil?
      handle = CApi::rcl_get_zero_initialized_timer()
      timer = Timer.new(handle, @node_handle, timer_period_sec, callback, callback_group, clock)
      @timers.append(timer)
      return timer
    end

    ##
    # Stop a timer
    # * +timer+ a timer created by create_timer
    def remove_timer(timer)
      @timers.remove timer
    end
  end
end
