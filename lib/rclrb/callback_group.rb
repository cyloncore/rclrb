module Rclrb
  ##
  # Base class for callback groups
  class CallbackGroupBase
    def initialize
      @members = []
    end
    def add member
      @members.append member
    end
  end
  ##
  # Callbacks in a mutually exclusive group are executed one at a time.
  class MutuallyExclusiveCallbackGroup < CallbackGroupBase
    def spin(wait_set = nil)
      Thread.new do
        threads = @members.each { |m| m.spin wait_set }
      end
    end
  end
  ##
  # Callbacks in a reentrant group are executed in parallel in seperate threads.
  class ReentrantCallbackGroup < CallbackGroupBase
    def spin(wait_set = nil)
      Thread.new do
        threads = @members.map { |m| Thread.new { m.spin wait_set } }
        threads.each { |x| x.join }
      end
    end
  end
end
