module Rclrb
  ##
  # An executor controls the threading model used to process callbacks. Callbacks are units of work
  # like subscription callbacks, timer callbacks, and service calls. An executor controls which threads
  # callbacks get executed in.
  class Executor
    def initialize(*nodes)
      @nodes = nodes
      @groups = nodes.map { |x| x.default_callback_group }
      @running = true
      @exitcode = 0
    end
    ##
    # Add a node to the executor
    def add_node(node)
      @nodes.append node
      @groups.append node.default_callback_group
    end
    def shutdown(exitcode)
      @exitcode = exitcode
      @running = false
      Rclrb.rcl_signal_guard_condition.trigger
    end
    ##
    # Spin continuously untill shutdown was requested
    def spin()
      while @running and not Rclrb.rcl_shutdown_requested?
        wait_set = WaitSet.new
        threads = @groups.map { |g| g.spin wait_set }
        threads.each { |t| t.join }
        wait_set.wait
      end
      return @exitcode
    end
  end
  ##
  # Spin the given nodes using Executor
  def Rclrb.spin(*nodes)
    e = Executor.new *nodes
    return e.spin
  end
end
