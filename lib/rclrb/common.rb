
def Object.rclrb_finalize_with *ivars_names, &block
  old_init = instance_method :initialize

  define_method :initialize do |*args|
    old_init.bind(self).call *args
    ivars = ivars_names.map { |ivar| instance_variable_get(ivar) }
    ObjectSpace.define_finalizer self, Object.rclrb_create_finalizer(block, ivars)
  end
  private_class_method def Object.rclrb_create_finalizer block, ivars
    return proc { block.call(*ivars) }
  end
end

module Rclrb
  ##
  # Base class for any exception triggered by Rclrb
  class Error < StandardError
  end
  ##
  # An unhandled error occured in the Rcl library. This is generally indicative of a bug in Rclrb. 
  class RclError < Error
  end
  ##
  # An error occured while parsing a message, service or action definiton.
  class ParseError < Error
  end
  ##
  # A call to service was interrupted
  class InterruptedClientCall < Error
  end
  ##
  # Failed to get answer to a service call
  class ServiceCallFailed < Error
  end
  ##
  # Publisher.call or Client.call was called with the wrong type of message.
  class InvalidMessageTypeError < Error
    def initialize got, expected
      super "Invalid message type got '#{got}' expected '#{expected}"
    end
  end
end
