module Rclrb
  ##
  # This module contains classes used to handle ROS messages.
  # The content of Fields is internal API to rclrb and should not be directly called and is subject
  # to change.
  module Fields
    # This function is used for building generic parse_ros_message_array function, that will read array elements one-by-one and call block to parse them
    def Fields.__generic_parse_array data, length, field_size, &block
      array = []
      for i in 0...length
        ptr = data + i * field_size
        array.append block.call(ptr)
      end
      return array
    end
    # This function is used for building generic fill_ros_message_array function, that will write array elements one-by-one and call block to add them
    def Fields.__generic_fill_ros_message_array array_pointer, value, field_size, &block
      for i in 0...value.length
        block.call array_pointer + i * field_size, value[i]
      end
    end


    class AtomicField
      attr_reader :init_value, :field_name, :ffi_type
      def initialize(init_value, field_name, ffi_type, array_reader, array_writter)
        @init_value = init_value
        @field_name    = "Rclrb::Fields::#{field_name}"
        @ffi_type      = ffi_type
        @memory_size   = FFI::type_size @ffi_type
        @array_reader  = array_reader
        @array_writter = array_writter
      end
      def create_from_dict v
        return v
      end
      def array_field()
        @array_field = ArrayField.new(self) unless defined?(@array_field)
        return @array_field
      end
      def parse_ros_message(data)
        return data
      end
      def parse_ros_message_array(data, length)
        if @array_reader
          return data.send @array_reader, 0, length
        else
          return Fields.__generic_parse_array(data, length, self.ffi_size) { |ptr| ptr.get(@ffi_type, 0) }
        end
      end
      def fill_ros_message(data, index, value)
        data[index] = value
      end
      def fill_ros_message_array(array_pointer, value)
        if @array_writter
          return array_pointer.send @array_writter, 0, value
        else
          Fields.__generic_fill_ros_message_array(array_pointer, value, self.ffi_size) { |ptr, value| ptr.put @ffi_type, 0, value }
        end
      end
      def destroy_ros_message(data)
      end
      def ffi_size()
        return @memory_size
      end
      def is_atomic
        return true
      end
    end
    class StringField
      def StringField.init_value()
        return "\"\""
      end
      def StringField.field_name()
        return "Rclrb::Fields::StringField"
      end
      def StringField.create_from_dict v
        return v
      end
      def StringField.parse_ros_message(data)
        length =  data[:length]
        return data[:string].get_string(0, length)
      end
      def StringField.parse_ros_message_array(data, length)
        return Fields.__generic_parse_array(data, length, StringField.ffi_size) { |ptr| StringField.parse_ros_message FFIType.new(ptr) }
      end
      def StringField.__fill_ros_message(ffi_struct, value)
        str_pointer = Rclrb.rcl_allocate value.length + 1
        ffi_struct[:string] = str_pointer
        str_pointer.put_string(0, value)
        str_pointer.put_uint8(value.length, 0)
        ffi_struct[:length] = value.length
        ffi_struct[:capacity] = value.length + 1
      end
      def StringField.fill_ros_message(data, index, value)
        StringField.__fill_ros_message(data[index], value)
      end
      def StringField.fill_ros_message_array(array_pointer, value)
        Fields.__generic_fill_ros_message_array(array_pointer, value, StringField.ffi_size) { |ptr, value| StringField.__fill_ros_message(ffi_type.new(ptr), value) }
      end
      def StringField.destroy_ros_message(data)
        if data.kind_of? FFIType
          Rclrb.rcl_deallocate data[:string]
        else
          return StringField.destroy_ros_message FFIType.new(data)
        end
      end
      def StringField.array_field()
        @@array_field = ArrayField.new(StringField) unless defined?(@@array_field)
        return @@array_field
      end
      def StringField.ffi_type
        return FFIType
      end
      def StringField.ffi_size
        return FFIType.size
      end
      def StringField.is_atomic
        return false
      end
      class FFIType < FFI::Struct
        layout :string, :pointer, :length, :size_t, :capacity, :size_t
      end
    end
    class TimeField
      def TimeField.init_value()
        return "Rclrb::Time.new()"
      end
      def TimeField.field_name()
        return "Rclrb::Fields::TimeField"
      end
      def TimeField.create_from_dict v
        return Time.from_sec_nsec v['sec'], v['nsec']
      end
      def TimeField.parse_ros_message(data)
        return Time.from_sec_nsec data[:sec], data[:nsec]
      end
      def TimeField.__fill_ros_message(ffi_struct, value)
        s, ns = value.to_sec_nsec
        ffi_struct[:sec] = s
        ffi_struct[:nsec] = ns
      end
      def TimeField.fill_ros_message(data, index, value)
        TimeField.__fill_ros_message(data[index], value)
      end
      def TimeField.fill_ros_message_array(array_pointer, value)
        Fields.__generic_fill_ros_message_array(array_pointer, value, TimeField.ffi_size) { |ptr, value| TimeField.__fill_ros_message(ffi_type.new(ptr), value) }
      end

      def TimeField.destroy_ros_message(data)
      end
      def TimeField.array_field()
        @@array_field = ArrayField.new(TimeField) unless defined?(@@array_field)
        return @@array_field
      end
      def TimeField.ffi_type
        return FFIType
      end
      def TimeField.ffi_size
        return FFIType.size
      end
      def TimeField.is_atomic
        return false
      end
      class FFIType < FFI::Struct
        layout :sec, :int32, :nsec, :uint32
      end
    end
    class ArrayField
      def initialize(field)
        @field = field
      end
      def init_value()
        return "[]"
      end
      def field_name()
        return "#{@field.field_name}.array_field()"
      end
      def create_from_dict v
        raise RclError.new "Expected Array got '#{v}' of type '#{v.class}'" unless v.is_a? Array
        return v.map { |x| @field.create_from_dict x }
      end
      def parse_ros_message(data)
        return @field.parse_ros_message_array data[:array], data[:length]
      end
      def fill_ros_message(data, index, value)
        array_pointer = Rclrb.rcl_allocate value.length * @field.ffi_size
        @field.fill_ros_message_array array_pointer, value
        arr = data[index]
        arr[:array] = array_pointer
        arr[:length] = value.length
        arr[:capacity] = value.length
      end
      SIZE = CApi::POINTER_SIZE + 2 * CApi::SIZE_T_SIZE
      def ros_size()
        return SIZE
      end
      def destroy_ros_message(data)
        length =  data[:length]
        unless @field.is_atomic
          for i in 0...length
            @field.destroy_ros_message data[:array] + i * @field.ffi_size
          end
        end
        Rclrb.rcl_deallocate data[:array]
      end
      def is_atomic
        return false
      end
      def ffi_type
        return FFIType
      end
      class FFIType < FFI::Struct
        layout :array, :pointer, :length, :size_t, :capacity, :size_t
      end
    end
    Float64Field = AtomicField.new "0.0", "Float64Field", :double, :get_array_of_float64, :put_array_of_float64
    Float32Field = AtomicField.new "0.0", "Float32Field", :float, :get_array_of_float32, :put_array_of_float32
    ByteField = AtomicField.new "0", "ByteField", :uint8, :get_array_of_uint8, :put_array_of_uint8
    CharField = AtomicField.new "0", "CharField", :uint8, :get_array_of_uint8, :put_array_of_uint8
    BoolField = AtomicField.new "false", "BoolField", :bool, nil, nil
    Int8Field = AtomicField.new "0", "Int8Field", :int8, :get_array_of_int8, :put_array_of_int8
    Int16Field = AtomicField.new "0", "Int16Field", :int16, :get_array_of_int16, :put_array_of_int16
    Int32Field = AtomicField.new "0", "Int32Field", :int32, :get_array_of_int32, :put_array_of_int32
    Int64Field = AtomicField.new "0", "Int64Field", :int64, :get_array_of_int64, :put_array_of_int64
    Uint8Field = AtomicField.new "0", "Uint8Field", :uint8, :get_array_of_uint8, :put_array_of_uint8
    Uint16Field = AtomicField.new "0", "Uint16Field", :uint16, :get_array_of_uint16, :put_array_of_uint16
    Uint32Field = AtomicField.new "0", "Uint32Field", :uint32, :get_array_of_uint32, :put_array_of_uint32
    Uint64Field = AtomicField.new "0", "Uint64Field", :uint64, :get_array_of_uint64, :put_array_of_uint64

    AtomicFields = { "float64" => Float64Field,
        "float32" => Float32Field,
        "char" => CharField,
        "byte" => ByteField,
        "bool" => BoolField,
        "int8" => Int8Field,
        "int16" => Int16Field,
        "int32" => Int32Field,
        "int64" => Int64Field,
        "uint8" => Uint8Field,
        "uint16" => Uint16Field,
        "uint32" => Uint32Field,
        "uint64" => Uint64Field,
        "string" => StringField,
        "builtin_interfaces/Time"   => TimeField }

  end
end
