module Rclrb

  ##
  # Initialise Rclrb with the given arguments
  def Rclrb.init(arguments: [])
    @@initOptions = CApi.rcl_get_zero_initialized_init_options()
    @@context = CApi.rcl_get_zero_initialized_context()
    @@allocator = CApi.rcutils_get_default_allocator
    CApi.handle_result(CApi.rcl_init_options_init(@@initOptions, @@allocator))

    # Prepare arguments
    p_values = nil
    if arguments.length > 0
      strings = arguments.map { |k| FFI::MemoryPointer.from_string(k.to_s) }
      p_values = FFI::MemoryPointer.new(:pointer, arguments.size + 1)
      p_values.write_array_of_pointer(strings)
    end

    # Call init
    CApi.handle_result(CApi.rcl_init(arguments.length, p_values, @@initOptions, @@context))

    # Remove from arguments the ros arguments
    idx = 0
    removing = false
    while idx < arguments.length
      if removing
        if arguments[idx] == "--"
          arguments.delete_at idx
          break
        end
        arguments.delete_at idx
      elsif arguments[idx] == "--ros-args"
        removing = true
      else
        idx += 1
      end
    end

    # Guard condition to shutdown execution when shutdown is called
    @@signal_guard_condition = GuardCondition.new
    @@shutdown_requested = false
    
    # Setup signal to catch ctr+c
    Rclrb.setup_signal
  end
  def Rclrb.setup_signal
    Signal.trap("INT") {
      puts "Interrupts called..."
      Rclrb.shutdown()
    }
  end
  ##
  # Terminate Rclrb
  def Rclrb.shutdown
    @@shutdown_requested = true
    @@signal_guard_condition.trigger
  end
  def Rclrb.rcl_context()
    return @@context
  end
  def Rclrb.rcl_signal_guard_condition
    return @@signal_guard_condition
  end
  def Rclrb.rcl_shutdown_requested?
    return @@shutdown_requested
  end
  def Rclrb.rcl_allocator
    return @@allocator
  end
  def Rclrb.rcl_allocate(size)
    return @@allocator[:allocate].call size, @@allocator[:state]
  end
  def Rclrb.rcl_deallocate(ptr)
    @@allocator[:deallocate].call ptr, @@allocator[:state]
  end
end
