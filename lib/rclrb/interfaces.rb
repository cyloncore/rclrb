module Rclrb
  ##
  # This module contains classes used to generate package to handle ROS messages.
  # The content of Interfaces is internal API to rclrb and should not be directly called and is subject
  # to change.
  module Interfaces
    def Interfaces.__parse_msg_srv_act_file package_name, path, filename, interface_type, interface_type_camel
      definition = {}
      definition[:name] = filename[0, filename.length - 4]
      definition[:class_name] = "#{Rclrb.camelize(package_name)}::#{interface_type_camel}::#{definition[:name]}"
      line_num = 0
      fieldss = []
      fields = []
      constantss = []
      constants = []
      depends = []
      ext_depends = []
      File.open("#{path}/#{interface_type}/#{filename}").each do |line|
        line_num += 1
        # Remove comment
        comment_index = line.index('#')
        line = line[0, comment_index] unless comment_index.nil?
        if line.index('=') != nil
          l = line.split('=')
          if l.length == 2
            l2 = l[0].split(' ')
            if l2.length == 2
              constants.append "#{l2[1].upcase} = #{l[1]}"
            else
              raise ParseError.new "Cannot parse '#{path}/#{interface_type}/#{filename}', invalid constant definition on line number #{line_num}"
            end
          else
            raise ParseError.new "Cannot parse '#{path}/#{interface_type}/#{filename}', invalid constant definition on line number #{line_num}"
          end
        else
          l = line.split(' ', ).reject { |c| c.empty?}
          if l.length == 2 or l.length == 3
            type = l[0]
            name = l[1]
            array_index = type.index(/\[\d*\]/)
            if array_index.nil?
              base_type = type
              is_array = false
            else
              base_type = type[0, array_index]
              is_array = true
            end

            if Fields::AtomicFields.include? base_type
              field_def = Fields::AtomicFields[base_type].field_name
              if l.length == 3
                field_init_value = l[2]
              else
                field_init_value = Fields::AtomicFields[base_type].init_value
              end
            else
              if base_type == "Header"
                field_def = "StdMsgs::#{interface_type_camel}::Header"
                field_init_value = "StdMsgs::Header.new"
                if package_name != "std_msgs"
                  ext_depends.append "std_msgs/msg"
                end
              elsif base_type.index("/").nil?
                field_def = "#{Rclrb.camelize(package_name)}::Msg::#{base_type}"
                field_init_value = "#{field_def}.new"
                if interface_type != 'msg'
                  ext_depends.append "#{package_name}/msg"
                end
              else
                base_type_split = base_type.split "/"
                if base_type_split.length == 2
                  ext_depends.append "#{base_type_split[0]}/msg" unless base_type_split[0] == package_name
                  field_def = "#{Rclrb.camelize(base_type_split[0])}::Msg::#{base_type_split[1]}"
                  field_init_value = "#{field_def}.new"
                else
                  raise ParseError.new "Cannot parse '#{path}/#{interface_type}/#{filename}', invalid type #{base_type} at line number #{line_num}"
                end
              end
              depends.append field_def
            end
            if is_array
              field_def += ".array_field"
              field_init_value = []
            end
            fields.append({ :name => name, :field_def_name => field_def, :field_init_value => field_init_value })
          elsif l.length == 1 and l[0] = "---"
            fieldss.append(fields)
            constantss.append(constants)
            fields = []
            constants = []
          elsif l.length != 0
            raise ParseError.new "Cannot parse '#{path}/#{interface_type}/#{filename}', invalid line number #{line_num}"
          end
        end
      end
      fieldss.append(fields)
      constantss.append(constants)
      definition[:fields] = fieldss
      definition[:constants] = constantss
      definition[:depends] = depends
      definition[:ext_depends] = ext_depends.uniq
      return definition
    end
    def Interfaces.__fields_body(index, klass_name)
      str = <<-EOF
      attr_accessor <%= mdef[:fields][#{index}].map { |f| ":" + f[:name] } .join(" ,") %>
      <% mdef[:constants][#{index}].each do |constant| %>
      <%= constant %>
      <% end %>
      def initialize()
        <% mdef[:fields][#{index}].each do |field| %>
        @<%= field[:name] %> = <%= field[:field_init_value] %>
        <% end %>
      end
      def #{klass_name}.create_from_dict(dict)
        v = #{klass_name}.new
        <% mdef[:fields][#{index}].each do |field| %>
        if dict.include? '<%= field[:name] %>'
          v.<%= field[:name] %> = <%= field[:field_def_name] %>.create_from_dict(dict['<%= field[:name] %>'])
        end
        <% end %>
        return v
      end
      class FFIType < FFI::Struct
        layout <%= mdef[:fields][#{index}].map { |f| ":" + f[:name] + ", " + f[:field_def_name] + ".ffi_type" }.join(" ,") %><% if(mdef[:fields][#{index}].empty?) %> :__dummy__, :int <% end %>
      end

      def #{klass_name}.get_ffi_struct(data)
        if data.kind_of? FFIType
          return data
        else
          return FFIType.new data
        end
      end
      def #{klass_name}.parse_ros_message(data)
        msg = #{klass_name}.new
        ffi_struct = #{klass_name}.get_ffi_struct(data)
        <% mdef[:fields][#{index}].each do |field| %>
        msg.<%= field[:name] %> = <%= field[:field_def_name] %>.parse_ros_message ffi_struct[:<%= field[:name] %>]
        <% end %>
        return msg
      end
      def #{klass_name}.parse_ros_message_array(data, length)
        return Fields.__generic_parse_array(data, length, #{klass_name}.ffi_size) { |ptr| #{klass_name}.parse_ros_message(ptr) }
      end
      def #{klass_name}.destroy_ros_message(data)
        ffi_struct = #{klass_name}.get_ffi_struct(data)
        <% mdef[:fields][#{index}].each do |field| %>
        <%= field[:field_def_name] %>.destroy_ros_message(ffi_struct[:<%= field[:name] %>])
        <% end %>
      end
      def #{klass_name}.get_ros_message(msg)
        ffi_struct = FFIType.new
        #{klass_name}.__fill_ros_message(ffi_struct, msg)
        return ffi_struct
      end
      def #{klass_name}.fill_ros_message(ffi_struct, member, msg)
        #{klass_name}.__fill_ros_message(ffi_struct[member], msg)
      end
      def #{klass_name}.__fill_ros_message(ffi_struct, msg)
        <% mdef[:fields][#{index}].each do |field| %>
        <%= field[:field_def_name] %>.fill_ros_message(ffi_struct, :<%= field[:name] %>,  msg.<%= field[:name] %>)
        <% end %>
      end
      def #{klass_name}.fill_ros_message_array(array_pointer, value)
        Fields.__generic_fill_ros_message_array(array_pointer, value, #{klass_name}.ffi_size) { |ptr, value| #{klass_name}.__fill_ros_message(ffi_type.new(ptr), value) }
      end
      def #{klass_name}.ffi_type
        return FFIType
      end
      def #{klass_name}.ffi_size
        return FFIType.size
      end
      def #{klass_name}.is_atomic
        return false
      end
EOF
    end
    def Interfaces.load_definitions package_name
      is_msg_package = package_name.end_with? "/msg"
      is_srv_package = package_name.end_with? "/srv"
      if is_msg_package or is_srv_package
        ros_package_name = package_name[0, package_name.size - 4]
      else
        return false
      end
      module_name = Rclrb.camelize ros_package_name
      p = "#{`ros2 pkg prefix #{ros_package_name}`.chop}/share/#{ros_package_name}"
      return false unless File.directory?(p)
      if is_msg_package
        msg_defs_unsorted = Dir.glob("*.msg", base: p + "/msg").map { |name| Interfaces.__parse_msg_srv_act_file(ros_package_name, p, name, "msg", "Msg") }
        # msg_defs.sort! { |a| a[:depends].length }
        msg_defs = []
        current_index = 0
        while msg_defs_unsorted.length > 0
          if current_index >= msg_defs_unsorted.length
            current_index = 0
          end 
          add_to_msg_defs = true
          msg_defs_unsorted[current_index][:depends].each() { |x|
            if x.start_with? module_name
              found = false
              msg_defs.each() { |y|
                if y[:class_name] == x
                  found = true
                  break
                end
              }
              unless found
                add_to_msg_defs = false
                break
              end
            end
          }
          if add_to_msg_defs
            msg_defs.push msg_defs_unsorted[current_index]
            msg_defs_unsorted.delete_at current_index
          else
            current_index += 1
          end
        end

        msg_defs.each() do |mdef|
          mdef[:ext_depends].each() { |d| require d }
        end
        CApi.create_type_support_library(module_name, ros_package_name, "message", "msg", "RosidlMessageTypeSupportT", msg_defs.map {|mdef| mdef[:name]})
        template = ERB.new <<-EOF
module <%= module_name %>
  module Msg
    <% msg_defs.each do |mdef| %>
    class <%= mdef[:name] %>
      def <%= mdef[:name] %>.type_support()
        TypeSupportCApi::TypeSupport_msg_<%= mdef[:name] %>
      end
      #{Interfaces.__fields_body(0, "<%= mdef[:name] %>")}
      def <%= mdef[:name] %>.array_field()
        @@array_field = Fields::ArrayField.new(<%= mdef[:name] %>) unless defined?(@@array_field)
        return @@array_field
      end
    end
    <% end %>
  end
end
EOF
        # puts template.result(binding)
        Object.class_eval template.result(binding)
        return true
      elsif is_srv_package
        srv_defs = Dir.glob("*.srv", base: p + "/srv").map { |name| Interfaces.__parse_msg_srv_act_file(ros_package_name, p, name, "srv", "Srv") }
        srv_defs.each() do |mdef|
          mdef[:ext_depends].each() { |d| require d }
        end
        CApi.create_type_support_library module_name, ros_package_name, "service", "srv", "RosidlServiceTypeSupportT", srv_defs.map {|mdef| mdef[:name]}
        template = ERB.new <<-EOF
module <%= module_name %>
  module Srv
  <% srv_defs.each do |mdef| %>
    module <%= mdef[:name] %>
      def <%= mdef[:name] %>.type_support()
        TypeSupportCApi::TypeSupport_srv_<%= mdef[:name] %>
      end
      class Request
        #{Interfaces.__fields_body(0, "Request")}
      end
      class Response
        #{Interfaces.__fields_body(1, "Response")}
      end
    end
  <% end %>
  end
end
EOF
        Object.class_eval template.result(binding)
        return true
      end
      return false
    end
  end
end
