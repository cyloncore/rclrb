module Rclrb
  ##
  # Represent time.
  class Time
    ##
    # Initialise time, +time+ is given in seconds
    def initialize time = 0
      @time_sec = time
    end
    ##
    # Create a time objects from seconds
    def Time.from_sec sec
      return Time.new sec
    end
    ##
    # Create a time objects from seconds and nanoseconds
    def Time.from_sec_nsec sec, nsec
      return Time.new sec + nsec * 1e-9
    end
    ##
    # Return the time in seconds
    def to_sec
      return @time_sec
    end
    ##
    # Return the time in second and nanoseconds (as integers)
    def to_sec_nsec
      si = @time_sec.to_i
      return si, ((@time_sec - si) * 1e9).to_i
    end
  end
end
