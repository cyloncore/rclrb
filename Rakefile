require 'rake'

# rspec
require 'rspec/core/rake_task'

# Gem
require 'rubygems'
require 'rubygems/package_task'

# RDoc
require 'rdoc/task'

# Rclrb
require_relative 'lib/rclrb/version'

# Gemspec

GEMSPEC = Gem::Specification.new do |s|
  s.name        = "rclrb"
  s.version     = Rclrb::VERSION
  s.summary     = "Ruby bindings for Ros Communication Library (ROS2)"
  s.description = <<-DESC
Allows to communicate with ROS program.
DESC
  s.authors     = ["Cyrille Berger"]
  s.files       = Dir.glob("lib/**/*")
  s.homepage    = "https://gitlab.com/cyloncore/rclrb"
  s.license     = "MPL-2.0"
  s.extra_rdoc_files = Dir["README.md", "COPYING"]
  s.add_dependency "ffi", "~> 1.15"
  s.add_development_dependency "rspec", "~> 3.0"
end

# Tasks

# test: will run the test suite
# 

RSpec::Core::RakeTask.new(:test) 

Gem::PackageTask.new(GEMSPEC) do |pkg|
  pkg.need_zip = true
  pkg.need_tar = true
end

task :release => [:test, :repackage] do
end

RDoc::Task.new :documentation do |rdoc|
  rdoc.main = "README.md"
  rdoc.rdoc_files.include("README.md", "lib/*.rb", "lib/rclrb/*.rb")
  # rdoc.markup = "markdown"
  rdoc.options << "-x" << "lib/rclrb/capi.rb" << "-x" << "lib/rclrb/fields.rb" << "-x" << "lib/rclrb/interfaces.rb" << "-x" << "lib/rclrb/wait_set.rb" << "-x" << "lib/rclrb/internal.rb"
end
